﻿using GetContactDetails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Text;
using System.Threading.Tasks;

namespace nbs_mortgages_switcher_stub_customer_manager
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in code, svc and config file together.
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class Service1 : GetSummaryIPDetailsUsingIPInfoWSSoap
    {
        public Task<GetSummaryIndividualDetailsUsingIPInfoResponse> GetSummaryIndividualDetailsUsingIPInfoAsync(GetSummaryIndividualDetailsUsingIPInfoRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<GetSummaryIndividualDetailsUsingNINoResponse> GetSummaryIndividualDetailsUsingNINoAsync(GetSummaryIndividualDetailsUsingNINoRequest request)
        {
            throw new NotImplementedException();
        }

        public Task<GetSummaryIPDetailsUsingIPIdResponse> GetSummaryIPDetailsUsingIPIdAsync(GetSummaryIPDetailsUsingIPIdRequest request)
        {
            var response = Task.FromResult<GetSummaryIPDetailsUsingIPIdResponse>(new GetSummaryIPDetailsUsingIPIdResponse
            {
                dataContext = new DataContext(),
                GetSummaryIPDetailsUsingIPIdResult = new IPSearchResult { IdvGivenName1 = "Sumit Test"}
            });

            return response;
        }

        public Task<GetSummaryOrganisationDetailsUsingNameResponse> GetSummaryOrganisationDetailsUsingNameAsync(GetSummaryOrganisationDetailsUsingNameRequest request)
        {
            throw new NotImplementedException();
        }
    }
}
